#!/bin/bash
source ~/.bashrc
module load plink

#first filter the individual with genotyping rate less than 90 percent
plink --cow --file taurine_african_indicine_autosomal --mind 0.1 --recode --out cattle_mind01

#Replace random family name with pop id
awk 'NR==FNR{a[$1]=$3;next}{if($2 in a){$1=a[$2];print;next}else;print}' pop_list_short_ids.txt cattle_mind01.ped>cattle_fam_mind01.ped

#keep specified number of samples per population in a ped file
python extract_pop.py family_more_than_3_indi.txt cattle_fam_mind01.ped cattle_sub_fam_mind01.ped
mv cattle_mind01.map cattle_sub_fam_mind01.map

#filter alleles with genotyping rate less than 95% and minor allele frequency less than 1%
plink --cow --file cattle_sub_fam_mind01 --geno 0.05 --maf 0.01 --recode --out cattle_mind01geno005maf001

#bovine HD snp array contains several duplicate SNP ids, following script list such ids
python remove_dupicates.py cattle_mind01geno005maf001.map dupli_snp_id.txt

#exclude these dupli id from the ped file
plink --cow --file cattle_mind01geno005maf001 --exclude dupli_snp_id.txt --recode --out cattle_mind01geno005maf001RemDup

