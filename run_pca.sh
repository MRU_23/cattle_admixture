#!/bin/bash
source ~/.bashrc

#replace breed id with pop id before running PCA
awk 'NR==FNR{a[$1]=$2;next}$1 in a{$1=a[$1];print}' family_id.txt cattle_mind01geno005maf001RemDup.fam > cattle_mind01geno005maf001RemDup_1.fam

#replace file name so that it satifies the reqirment of PCA.r script
mv cattle_mind01geno005maf001RemDup.fam cattle_mind01geno005maf001RemDup_ori.fam
mv cattle_mind01geno005maf001RemDup_1.fam cattle_mind01geno005maf001RemDup.fam

#run PCA.r script requires R packages: gdsfmat,SNPRelate,ggplot2,lattice
Rscript pca.r domestic_cattle> domestic_cattle_snp_array_PCA_log.txt

