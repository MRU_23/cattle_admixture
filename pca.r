#!/usr/bin/env Rscript
library("gdsfmt")
library("SNPRelate")
library("ggplot2")
library("lattice")
args=commandArgs(TRUE)

#usage Rscript PCA.r input file 

runPCA=function(stem){
    bedB.fn=paste(stem,".bed",sep="")
    bedf.fn=paste(stem,".fam",sep="")
    bedBi.fn=paste(stem,".bim",sep="")
    pop.colors=c(colors()[91],colors()[142],colors()[137],colors()[99],colors()[73],colors()[24],colors()[68],colors()[201],colors()[43],colors()[50],colors()[116],colors()[514])
    snpgdsBED2GDS(bedB.fn,bedf.fn,bedBi.fn,family=TRUE,"cattle.gds")
    genofile=snpgdsOpen("cattle.gds")
    pca=snpgdsPCA(genofile,num.thread=1,autosome.only=FALSE)
    pc.percent <- pca$varprop*100
    print(pc.percent)
    sample.id=pca$sample.id
    EV1=pca$eigenvect[,1]
    EV2=pca$eigenvect[,2]
    tab=data.frame(sample.id,EV1,EV2,stringsAsFactors=FALSE)
    pop.id=read.gdsn(index.gdsn(genofile,"sample.annot/family"))
    jpeg("cattle_pca.jpeg",width=4,height=4,unit='in',res=300)
    print({ggplot(tab,aes(x=EV1,y=EV2,colour=pop.id))+geom_point(size=1)+scale_colour_manual(values=pop.colors)})
    #print({ggplot(tab,aes(x=EV1,y=EV2,colour=pop.id))+geom_point(size=1)})
    dev.off()
    write.table(tab,"pcaResult.txt",sep="\t",row.names=FALSE,quote=FALSE)
    }
runPCA(args[1])
