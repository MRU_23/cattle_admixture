#!/bin/bash
source ~/.bashrc
module load plink

#following codes will report overlap SNP between yak and cattle map and bim files, resp. 
awk 'NR==FNR{a[$4][$1]=$2;next}$4 in a{for (j in a[$4]){if(j==$1){print;break}else;}}' cattle_mind01geno005maf001RemDup.bim yak_min4max30RmDupPosGeno0.map > yak_cattle_overlap.map

#following codes will report overlap SNP between aurochs an cattle map and bim files, resp.
awk 'NR==FNR{a[$4][$1]=$2;next}$4 in a{for (j in a[$4]){if(j==$1){print;break}else;}}' cattle_mind01geno005maf001RemDup.bim aurochs_min4max30RmDupPosGeno0.map > aurochs_cattle_overlap.map

#following codes will report overlap between cattle, yak and aurochs bim file
awk 'NR==FNR{a[$1];next}$1 in a{print}' yak_cattle_overlap.map aurochs_cattle_overlap.map > yak_cattle_aurochs_overlap_snps.txt

#extract common SNPs from yak and aurochs plink files
plink --cow --file yak_min4max30RmDupPosGeno0 --extract yak_cattle_aurochs_overlap_snps.txt --recode --out yak_common
plink --cow --file aurochs_min4max30RmDupPosGeno0 --extract yak_cattle_aurochs_overlap_snps.txt --recode --out aurochs_common

#extract common SNPs from cattle file
awk 'NR==FNR{a[$4][$1]=$2;next}$2 in a{for(j in a[$2]){if (j==substr($1,4)){print a[$2][j];break}else;}}' cattle_mind01geno005maf001RemDup.bim FS=":" yak_cattle_aurochs_overlap_snps.txt > yak_cattle_aurochs_overlap_snpids.txt
plink --cow --bfile cattle_mind01geno005maf001RemDup --extract yak_cattle_aurochs_overlap_snpids.txt --make-bed --out cattle_common

#allot SNP array id to the aurochs and yak ped files
awk 'NR==FNR{a[$4][$1]=$2;next}$2 in a{for(j in a[$2]){if (j==substr($1,4)){print $1":"$2,a[$2][j];break}else;}}' cattle_mind01geno005maf001RemDup.bim FS=":" yak_cattle_aurochs_overlap_snps.txt > yak_cattle_aurochs_overlap_pos_snpids.txt
awk 'NR==FNR{a[$1]=$2;next}$2 in a{$2=a[$2];print}' yak_cattle_aurochs_overlap_pos_snpids.txt aurochs_common.map > aurochs_common_array_id.map
awk 'NR==FNR{a[$1]=$2;next}$2 in a{$2=a[$2];print}' yak_cattle_aurochs_overlap_pos_snpids.txt yak_common.map > yak_common_array_id.map
mv aurochs_common.ped aurochs_common_array_id.ped
mv yak_common.ped yak_common_array_id.ped

#convert yak and aurochs ped file to bed file
plink --cow --file yak_common_array_id --make-bed --out yak_common_binary
plink --cow --file aurochs_common_array_id --make-bed --out aurochs_common_binary

#merge yak and aurochs bim file
plink --cow --bfile aurochs_common_binary --bmerge yak_common_binary --make-bed --out yak_cattle_common_merged

#flipped missnp in aurochs binary file
plink --cow --bfile aurochs_common_binary --flip yak_cattle_common_merged-merge.missnp --make-bed --out aurochs_common_binary_flipped

#merge yak and aurochs bim file (after flipping)
plink --cow --bfile aurochs_common_binary_flipped --bmerge yak_common_binary --make-bed --out yak_aurochs_flipped_common_merged

#remove offending SNPs (#95)
plink --cow --bfile aurochs_common_binary_flipped --exclude yak_aurochs_flipped_common_merged-merge.missnp --make-bed --out aurochs_common_flipped_exclude_missnp
plink --cow -bfile yak_common_binary --exclude yak_aurochs_flipped_common_merged-merge.missnp --make-bed --out yak_common_binary_exclude_missnp

#merge yak and aurochs bim file (after removing offending SNPs)
plink --cow --bfile aurochs_common_flipped_exclude_missnp --bmerge yak_common_binary_exclude_missnp --make-bed --out aurochs_yak_merged

#extract merged SNP id (from yak and aurochs) from cattle bed file
awk '{print $2}' aurochs_yak_merged.bim > aurochs_yak_merged_snp_ids.txt
plink --cow --bfile cattle_common --extract aurochs_yak_merged_snp_ids.txt --make-bed --out cattle_common_merged

#merge cattle and aurochs-yak merged bed files
plink --cow --bfile cattle_common_merged --bmerge aurochs_yak_merged --make-bed --out cattle_common_aurochs_yak

#flipped offending SNPs in aurochs_yak_merged
plink --cow --bfile aurochs_yak_merged --flip cattle_common_aurochs_yak-merge.missnp --make-bed --out aurochs_yak_merged_flipped_2

#merged cattle and aurochs-yak merged flipped bed files
plink --cow --bfile cattle_common_merged --bmerge aurochs_yak_merged_flipped_2 --make-bed --out cattle_common_aurochs_yak_flipped_2

#remove offending SNPs (#1095)
plink --cow --bfile aurochs_yak_merged_flipped_2 --exclude cattle_common_aurochs_yak_flipped_2-merge.missnp --make-bed --out aurochs_yak_merged_flipped_exclude_missnp_2
plink --cow --bfile cattle_common_merged --exclude cattle_common_aurochs_yak_flipped_2-merge.missnp --make-bed --out cattle_common_merge_exclude_missnp

#merge cattle and aurochs-yak bed file datasets
plink --cow --bfile cattle_common_merge_exclude_missnp --bmerge aurochs_yak_merged_flipped_exclude_missnp_2 --make-bed --out cattle_aurochs_yak




















